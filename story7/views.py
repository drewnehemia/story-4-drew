from django.shortcuts import render

# Create your views here.
def experiences(request):
    return render(request, 'story7/experiences.html')