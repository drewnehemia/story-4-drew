from django.test import TestCase, Client
from .models import Kegiatan, Attendee

# Create your tests here.
class Tester(TestCase):
    def setUp(self):
        self.activity = "test"
        self.a_kegiatan = Kegiatan(activity=self.activity)

    def test_url_kegiatan(self):
        response = Client().get('/story6/kegiatan')
        self.assertEquals(response.status_code,200)

    def test_url_kegiatan_add(self):
        response = Client().get('/story6/add-kegiatan')
        self.assertEquals(response.status_code,200)

    def test_url_member_add(self):
        response = Client().get('/story6/add-member')
        self.assertEquals(response.status_code,200)

    def test_kegiatan_template(self):
        response = Client().get('/story6/kegiatan')
        self.assertTemplateUsed(response, 'story6/kegiatan.html')

    def test_kegiatan_add_template(self):
        response = Client().get('/story6/add-kegiatan')
        self.assertTemplateUsed(response, 'story6/kegiatan-add.html')
    
    def test_member_add_template(self):
        response = Client().get('/story6/add-member')
        self.assertTemplateUsed(response, 'story6/member-add.html')

    #model test
    def test_model_kegiatan(self):
        Kegiatan.objects.create(activity = "this is an activity")
        counter = Kegiatan.objects.all().count()
        self.assertEqual(counter,1)

    def test_model_kegiatan_activity(self):
        keg = Kegiatan.objects.create(activity="test")
        mem = Attendee.objects.create(attendee="tester", kegiatan=keg)
        self.assertEqual(mem.__str__(),mem.attendee)
        self.assertEqual(keg.__str__(),keg.activity)
        self.assertEqual("/story6/kegiatan",mem.get_absolute_url())

    def test_get_absolute_url(self):
        self.assertEqual("/story6/kegiatan",self.a_kegiatan.get_absolute_url())