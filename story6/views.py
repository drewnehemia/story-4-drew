from django.shortcuts import render
from django.views.generic import ListView, DetailView, DeleteView, CreateView
from .models import Kegiatan, Attendee


# Create your views here.
class KegiatanView(ListView):
    context_object_name = 'kegiatan_list'    
    template_name = 'story6/kegiatan.html'
    queryset = Kegiatan.objects.all()

    def get_context_data(self, **kwargs):
        context = super(KegiatanView, self).get_context_data(**kwargs)
        context['attendees'] = Attendee.objects.all()
        return context

class AddKegiatanView(CreateView):
    model = Kegiatan
    template_name = 'story6/kegiatan-add.html'
    fields = '__all__'

class AddMemberView(CreateView):
    model = Attendee
    template_name = 'story6/member-add.html'
    fields = '__all__'              