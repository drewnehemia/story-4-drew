from django.urls import path
from . import views
from .views import MatkulView, MatkulDetail, MatkulDelete

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('about', views.about, name='about'),
    path('projects', views.projects, name='projects'),
    path('matakuliah', MatkulView.as_view(), name='matakuliah'),
    path('detail/<int:pk>/', MatkulDetail.as_view(), name='matkul-detail'),
    path('matakuliah-add', views.matkul_add, name='matakuliah-add'),
    path('detail/<int:pk>/remove', MatkulDelete.as_view(), name='matkul-delete'),
]